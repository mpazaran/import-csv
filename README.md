IMPORT CSV TO MYSQL USING PHP

EXAMPLE:
	Basic usage
	importCsv -fMY_FILE_DATA.csv -dMY_DATABASE

USAGE:
	importCsv -f {file} -d {database} [OPTIONS]

	 -h Help, show this information
	 -t Target table name, default is php_csv_importer
	 -b Perform commit each records, default is 1000
	 -D Database hostname, default is localhost
	 -U Database user, default is root
	 -P Database password, default is empty
	 -s Field delimiter, default is comma (,)
	 -e Text enclosure, default is double quotes (")
	 -m Method
		a	DEFAULT, Adds new records.
		d	Delete the table if exists.
		t	Truncates the table if exists.